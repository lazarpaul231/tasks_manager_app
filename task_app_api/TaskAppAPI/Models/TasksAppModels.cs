namespace TaskAppAPI.Models
{
    public class Tasks
    {
        public int ID { get; set; }
        public string TaskName { get; set; }
        public bool Checked { get; set; }
    }
}
