import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios';

import Task from './Task';
import SearchBar from './SearchBar';

const API_URL = 'http://localhost:5159/api/TasksApp';

function Tasks() {
  const [tasks, setTasks] = useState([]);
  const [newTaskName, setNewTaskName] = useState('');
  const [keyword, setKeyword] = useState('');
  const [filteredTasks, setFilteredTasks] = useState([]);

  const getTasks = useCallback(() => {
    axios.get(`${API_URL}/GetTasks`)
    .then(response => {
      setTasks(response.data);
    });
  }, []);

  useEffect(() => {
    getTasks();
  }, [getTasks]);

  useEffect(() => {
    setFilteredTasks(tasks.filter(task => task.TaskName.toLowerCase().includes(keyword.toLowerCase())));
  }, [tasks, keyword]);

  const addTask = useCallback(() => {
    if (!newTaskName.trim()) {
      alert('Please enter a task name');
      return;
    }
  
    const formData = new FormData();
    formData.append('newTaskName', newTaskName);
  
    axios.post(`${API_URL}/AddTasks`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    .then(() => {
      getTasks();
      setNewTaskName('');
    })
  }, [newTaskName, getTasks]);
  

  const updateTask = useCallback((task) => {
    axios.put(`${API_URL}/UpdateTask`, task)
      .then(() => {
        getTasks();
      });
  }, [getTasks]);

  const deleteTask = useCallback((id) => {
    axios.delete(`${API_URL}/DeleteTasks?id=${id}`)
      .then(() => {
        getTasks();
      });
  }, [getTasks]);

  const uncheckAll = useCallback(() => {
    const uncheckedTasks = tasks.map(task => task.Checked ? { ...task, Checked: false } : task);
    setTasks(uncheckedTasks);
  
    uncheckedTasks.forEach(task => {
      if (!task.Checked) {
        updateTask(task);
      }
    });
  }, [tasks, updateTask]);

  return (
    <div className='container'>
      <h1>Your tasks for today</h1>
      <SearchBar keyword={keyword} setKeyword={setKeyword} />
      <h4>Check off items once you have completed them</h4>
      <div className='tasksTable'>
        {filteredTasks.map((task, index) => (
          <Task key={task.ID} task={task} index={index} updateTask={updateTask} deleteTask={deleteTask} />
        ))}
      </div>
      <button className='uncheckedBtn' onClick={uncheckAll}>
        <img src="/images/checked.svg" alt='' className="btnIcon" />
        Uncheck all tasks
      </button>
      <h3>Add a new task</h3>
      <div className='addTask'>
        <input 
          type="text" 
          placeholder="Insert a new task"
          value={newTaskName} 
          onChange={e => setNewTaskName(e.target.value)} />
        <button className='addBtn' onClick={addTask}>
          <img src="/images/add.svg" alt='' className="btnIcon" />
          Add Task
        </button>
      </div>
    </div>
  );
}

export default Tasks;
