﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using TaskAppAPI.Models;


namespace TaskAppAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class TasksAppController : ControllerBase
    {
        private IConfiguration _configuration;
        public TasksAppController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        [Route("GetTasks")]
        public JsonResult GetTasks()
        {
            string query = "select * from dbo.tasks";
            DataTable table = new DataTable();
            string sqlDatasource = _configuration.GetConnectionString("tasksAppDBCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDatasource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }

        [HttpPost]
        [Route("AddTasks")]
        public JsonResult AddTasks([FromForm] string newTaskName)
        {
            string query = "insert into dbo.tasks (TaskName, Checked) values (@TaskName, 0)";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("tasksAppDBCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@TaskName", newTaskName);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Task Added Successfully");
        }

        [HttpPut]
        [Route("UpdateTask")]
        public JsonResult UpdateTask([FromBody] Tasks task)
        {
            string query = "UPDATE dbo.Tasks SET TaskName = @TaskName, Checked = @Checked WHERE ID = @ID";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("tasksAppDBCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ID", task.ID);
                    myCommand.Parameters.AddWithValue("@TaskName", task.TaskName);
                    myCommand.Parameters.AddWithValue("@Checked", task.Checked);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Task Updated Successfully");
        }


        [HttpDelete]
        [Route("DeleteTasks")]
        public JsonResult DeleteTasks(int id)
        {
            string query = "delete from dbo.tasks where id=@id";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("tasksAppDBCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@id", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Deleted Successfully");
        }

    }
}