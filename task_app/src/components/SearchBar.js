import React from 'react';

function SearchBar({ keyword, setKeyword }) {
  const handleSearch = (e) => {
    e.preventDefault();
    setKeyword(e.target.value);
  };

  return (
    <input
      type="search"
      value={keyword}
      placeholder="Search task..."
      onChange={handleSearch}
    />
  );
}

export default SearchBar;
