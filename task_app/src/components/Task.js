import React, { useState, useEffect, useCallback } from 'react';

function Task({ task, index, updateTask, deleteTask }) {

  const [isEditing, setIsEditing] = useState(false);
  const [editedTaskName, setEditedTaskName] = useState(task.TaskName);

  useEffect(() => {
    setEditedTaskName(task.TaskName);
  }, [task.TaskName]);

  const handleEdit = useCallback(() => {
    if (isEditing) {
      updateTask({ ...task, TaskName: editedTaskName });
    }
    setIsEditing(!isEditing);
  }, [isEditing, task, editedTaskName, updateTask]);

  return (
    <div key={task.ID} className='task'>
      <div className='taskContent'>
        <input className='taskContentCheckbox' type="checkbox" checked={task.Checked} onChange={() => updateTask({ ...task, Checked: !task.Checked })} />
        <p>{index + 1}.</p>
        {isEditing ? (
          <input type="text" value={editedTaskName} onChange={e => setEditedTaskName(e.target.value)} placeholder="Add task name"/>
        ) : (
          <p>{task.TaskName}</p>
        )}
      </div>
      <div className='taskBtns'>
        <button onClick={handleEdit} className='editBtn'> 
          <img src={isEditing ? '/images/delete.svg' : '/images/edit.svg'} alt='' className="btnIcon" />
          {isEditing ? 'Done' : 'Edit'}
        </button>
        <button onClick={() => deleteTask(task.ID)} className='deleteBtn'>
          <img src="/images/delete.svg" alt='' className="btnIcon" />
          Delete
        </button>
      </div>
    </div>
  );
}

export default Task;
